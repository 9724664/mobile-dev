package com.example.eden.imagebrowser;

import android.os.Parcelable;
import android.widget.ImageView;
import android.os.Parcel;

/**
 * Created by Eden on 20/09/2015.
 */
public class ImageParce implements Parcelable {

    private ImageView _image;
    private String _name;

    public ImageParce (ImageView image, String name)
    {
        _image = image;
        _name = name;
    }

    public ImageView getImage() {
        return _image;
    }

    public String getName() {
        return _name;
    }

    public int describeContents()
    {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags)
    {
        out.writeString(_name);
    }

    public static final Parcelable.Creator<ImageParce> CREATOR =
            new Parcelable.Creator<ImageParce>()
            {
                public ImageParce createFromParcel(Parcel in)
                {
                    return new ImageParce(in);
                }

                public ImageParce[] newArray(int size)
                {
                    return new ImageParce[size];
                }
            };

    /** Private constructor called internally only */
    private ImageParce(Parcel in)
    {
        _name = in.readString();
    }
}
