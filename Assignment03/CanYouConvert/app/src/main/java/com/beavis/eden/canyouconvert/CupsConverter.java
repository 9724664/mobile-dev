package com.beavis.eden.canyouconvert;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.util.Log;


public class CupsConverter extends ActionBarActivity {

    private boolean litres = false;
    private TextView textOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cups_converter);

        initUI(savedInstanceState);
    }


    public void ButtonClicked(View view)
    {
        //Button button = (Button) view;

        textOut = (TextView) findViewById(R.id.txtOutput);

        String sCups;
        String sTea;
        String sTable;

        if ( ((EditText)findViewById(R.id.cups)).getText() == null ||  ((EditText)findViewById(R.id.cups)).getText().toString().isEmpty())
        {
            sCups = "0.0";
        }
        else
        {
            sCups = ((EditText)findViewById(R.id.cups)).getText().toString();
        }

        if ( ((EditText)findViewById(R.id.teaspoons)).getText() == null || ((EditText)findViewById(R.id.teaspoons)).getText().toString().isEmpty() )
        {
            sTea = "0.0";
        }
        else
        {
            sTea = ((EditText)findViewById(R.id.teaspoons)).getText().toString();
        }

        if ( ((EditText)findViewById(R.id.tablespoons)).getText() == null || ((EditText)findViewById(R.id.tablespoons)).getText().toString().isEmpty())
        {
            sTable = "0.0";
        }
        else
        {
            sTable = ((EditText)findViewById(R.id.tablespoons)).getText().toString();
        }

        Double cups = Double.parseDouble(sCups);
        Double tea = Double.parseDouble(sTea);
        Double table = Double.parseDouble(sTable);

        cups = cups * 236.58;
        tea = tea * 4.92;
        table = table * 14.78;

        double result = cups + tea + table;

        if (litres == true)
        {
            result = result/1000;
            textOut.setText(round(result, 2) + " L");
        }
        else
        {
            textOut.setText(round(result, 2) + " ml");
        }
    }

    public void CheckboxClicked(View view)
    {
        if(litres == false)
        {
            litres = true;
        }
        else
        {
            litres = false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void initUI(Bundle state)
    {
        if (state == null) {
            return;
        }

        EditText  cups = ((EditText)findViewById(R.id.cups));
        EditText  teaspoons = ((EditText)findViewById(R.id.teaspoons));
        EditText  tablespoons = ((EditText)findViewById(R.id.tablespoons));
        CheckBox chkbox = ((CheckBox)findViewById(R.id.checkbox));

        cups.setText(state.getString("cups"));
        teaspoons.setText(state.getString("teaspoons"));
        tablespoons.setText(state.getString("tablespoons"));

        if (litres == false)
        {
            chkbox.setChecked(false);
        }
        else
        {
            chkbox.setChecked(true);
        }
    }

    protected void onSaveInstanceState(Bundle state)
    {
        String  cups = ((EditText)findViewById(R.id.cups)).getText().toString();
        String  teaspoons = ((EditText)findViewById(R.id.teaspoons)).getText().toString();
        String  tablespoons = ((EditText)findViewById(R.id.tablespoons)).getText().toString();

        state.putString("cups", cups);
        state.putString("teaspoons", teaspoons);
        state.putString("tablespoons", tablespoons);

        super.onSaveInstanceState(state);
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }
}
