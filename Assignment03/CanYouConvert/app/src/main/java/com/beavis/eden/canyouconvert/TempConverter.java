package com.beavis.eden.canyouconvert;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;


public class TempConverter extends ActionBarActivity {

    private TextView textOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temp_converter);

        initUI(savedInstanceState);
    }

    public void ConvertToF(View view)
    {

        textOut = (TextView) findViewById(R.id.txtOutput);
        String sTemp;

        if ( ((EditText)findViewById(R.id.temp)).getText() == null ||  ((EditText)findViewById(R.id.temp)).getText().toString().isEmpty())
        {
            sTemp = "0.0";
            textOut.setText("Invalid Input");
        }
        else
        {
            sTemp = ((EditText)findViewById(R.id.temp)).getText().toString();
            Double temp = Double.parseDouble(sTemp);

            double result = (temp * 1.8) + 32;
            textOut.setText(round(result, 2) + " F");
        }
    }

    public void ConvertToC(View view)
    {

        textOut = (TextView) findViewById(R.id.txtOutput);
        String sTemp;

        if ( ((EditText)findViewById(R.id.temp)).getText() == null ||  ((EditText)findViewById(R.id.temp)).getText().toString().isEmpty())
        {
            sTemp = "0.0";
            textOut.setText("Invalid Input");
        }
        else
        {
            sTemp = ((EditText)findViewById(R.id.temp)).getText().toString();
            Double temp = Double.parseDouble(sTemp);

            double result = (temp - 32) / 1.8;
            textOut.setText(round(result, 2) + " C");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_temp_converter, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    public void initUI(Bundle state)
    {
        if (state == null) {
            return;
        }

        EditText  temp = ((EditText)findViewById(R.id.temp));

        temp.setText(state.getString("temp"));
    }

    protected void onSaveInstanceState(Bundle state)
    {
        String  temp = ((EditText)findViewById(R.id.temp)).getText().toString();

        state.putString("temp", temp);

        super.onSaveInstanceState(state);
    }
}
