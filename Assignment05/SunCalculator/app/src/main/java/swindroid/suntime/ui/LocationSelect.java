package swindroid.suntime.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import swindroid.suntime.R;
import swindroid.suntime.calc.Location;
import swindroid.suntime.calc.LocationArrayAdapter;

public class LocationSelect extends Activity {

    private LocationArrayAdapter locationAdapter;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_select);

        InitUI();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //int pos = Integer.parseInt(((TextView) view).getTag().toString());
                Location newLocation = locationAdapter.getItem(i);

                Intent data = new Intent();
                data.putExtra("value", newLocation);
                if(getParent() == null)
                {
                    setResult(Activity.RESULT_OK, data);
                }
                else
                {
                    getParent().setResult(Activity.RESULT_OK, data);
                }
                finish();
            }
        });
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_location_select, menu);
        return true;
    }

    private void InitUI()
    {
        ArrayList<String[]> readList = new ArrayList<String[]>();
        InputStream inputStream = getResources().openRawResource(R.raw.au_locations);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        try
        {
            String csvLine;
            while((csvLine = reader.readLine()) != null)
            {
                String[] row = csvLine.split(",");
                readList.add(row);
            }
        }
        catch ( IOException ex)
        {
            throw new RuntimeException("Error reading file:" + ex);
        }
        finally
        {
            try
            {
                inputStream.close();
            }
            catch (IOException e)
            {
                throw new RuntimeException("Error closing input stream: " + e);
            }
        }

        ArrayList<Location> locations = new ArrayList<Location>();

        for (int i = 0; i < readList.size(); i++)
        {
            String[] row = readList.get(i);

            locations.add(new Location(row[0],Double.parseDouble(row[1]),Double.parseDouble(row[2]),row[3]));
        }

        locationAdapter = new LocationArrayAdapter(getApplicationContext(), R.layout.single_list_item);

        for(Location loc :  locations)
        {
            locationAdapter.add(loc);
        }

        listView = (ListView)findViewById(R.id.list);
        listView.setAdapter(locationAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
