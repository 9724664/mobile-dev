package swindroid.suntime.ui;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import swindroid.suntime.R;
import swindroid.suntime.calc.AstronomicalCalendar;
import swindroid.suntime.calc.GeoLocation;
import swindroid.suntime.calc.Location;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;
import android.widget.TextView;

public class Main extends Activity 
{
	GeoLocation currentLocation;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        initializeUI();
    }

	private void initializeUI()
	{
		DatePicker dp = (DatePicker) findViewById(R.id.datePicker);
		Calendar cal = Calendar.getInstance();
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH);
		int day = cal.get(Calendar.DAY_OF_MONTH);
		dp.init(year, month, day, dateChangeHandler); // setup initial values and reg. handler

		if (currentLocation == null)
		{
			TimeZone tz = TimeZone.getDefault();
			currentLocation = new GeoLocation("Melbourne", -37.50, 145.01, tz);
		}

		updateTime(year, month, day);
	}



	public void ButtonPushed(View v)
	{
		Intent newAct = new Intent(this, LocationSelect.class);
		startActivityForResult(newAct, 0);
	}

	private void updateTime(int year, int monthOfYear, int dayOfMonth)
	{
		//TimeZone tz = TimeZone.getDefault();
		//eoLocation geolocation = new GeoLocation("Melbourne", -37.50, 145.01, tz);
		AstronomicalCalendar ac = new AstronomicalCalendar(currentLocation);
		ac.getCalendar().set(year, monthOfYear, dayOfMonth);
		Date srise = ac.getSunrise();
		Date sset = ac.getSunset();
		
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		
		TextView sunriseTV = (TextView) findViewById(R.id.sunriseTimeTV);
		TextView sunsetTV = (TextView) findViewById(R.id.sunsetTimeTV);
		Log.d("SUNRISE Unformatted", srise+"");
		
		sunriseTV.setText(sdf.format(srise));
		sunsetTV.setText(sdf.format(sset));		
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent intent)
	{
		if(intent == null)
		{

		}
		else {
			Location temp = intent.getExtras().getParcelable("value");

			TimeZone tz = TimeZone.getTimeZone(temp.getTimezone());
			currentLocation = new GeoLocation(temp.getName(),temp.getLatitude(),temp.getLongitude(),tz);

			TextView tv = (TextView)findViewById(R.id.locationTV);
			tv.setText(temp.getName());

			initializeUI();
		}
	}

	OnDateChangedListener dateChangeHandler = new OnDateChangedListener()
	{
		public void onDateChanged(DatePicker dp, int year, int monthOfYear, int dayOfMonth)
		{
			updateTime(year, monthOfYear, dayOfMonth);
		}	
	};
	
}