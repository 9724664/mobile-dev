package com.example.eden.booklist;

import android.graphics.drawable.Drawable;
import android.media.Image;
import android.widget.ImageView;

/**
 * Created by Eden on 27/10/2015.
 */
public class Book {

    private Drawable _image;
    private String _title;
    private int _rating;

    public Book(Drawable image, String title, int rating)
    {
        _image = image;
        _title = title;
        _rating = rating;
    }

    public Drawable getImage(){return _image;}
    public String getTitle(){return _title;}
    public int getRating(){return _rating;}
}
