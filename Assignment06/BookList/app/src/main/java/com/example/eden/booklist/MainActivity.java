package com.example.eden.booklist;

import android.graphics.drawable.Drawable;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private ArrayList<Book> books;
    private BookArrayAdapter bookAdapter;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initBooks();
    }


    private void initBooks()
    {
        books = new ArrayList<Book>();
        Random rn = new Random();

        for (int i = 1; i < 11; i++)
        {
            int id = getResources().getIdentifier("@drawable/image" + i, null, getPackageName());
            Drawable temp = getResources().getDrawable(id);
                  //  temp.getDrawable(id);

            books.add(new Book(temp, "Book" + i, rn.nextInt(5) + 1));
        }

        bookAdapter = new BookArrayAdapter(getApplicationContext(), R.layout.single_list_item);

        for(Book b :  books)
        {
            bookAdapter.add(b);
        }

        listView = (ListView)findViewById(R.id.list);
        listView.setAdapter(bookAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
