package com.example.eden.booklist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eden on 27/10/2015.
 */
public class BookArrayAdapter extends ArrayAdapter<Book>
{
    private List<Book> bookList = new ArrayList<Book>();

    static class ItemViewHolder{
        ImageView image;
        TextView name;
    }

    public BookArrayAdapter (Context context, int resource)
    {
        super(context, resource);
    }

    public void add(Book object)
    {
        bookList.add((Book)object);
        super.add((Book)object);
    }

    public int getCount(){
        return super.getCount();
    }

    public Book getItem(int position)
    {
        return this.bookList.get(position);
    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        View row = convertView;
        ItemViewHolder viewHolder;

        if(row == null)
        {
            LayoutInflater inflater = (LayoutInflater)this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.single_list_item, parent, false);
            viewHolder = new ItemViewHolder();
            viewHolder.name = (TextView)row.findViewById(R.id.name);
            viewHolder.image = (ImageView)row.findViewById(R.id.image);
            row.setTag(viewHolder);
        }
        else
        {
            viewHolder = (ItemViewHolder)row.getTag();
        }

        Book book = getItem(position);
        viewHolder.image.setImageDrawable(book.getImage());

        viewHolder.name.setText("Title: " + book.getTitle() + "\n" + "Rating: " + book.getRating() + "/5");
        //viewHolder.name.setTag(position);
        return row;
    }
}
