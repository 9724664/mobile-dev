package swindroid.suntime.calc;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Eden on 11/10/2015.
 */
public class Location implements Parcelable
{
    String _name;
    double _latitude;
    double _longitude;
    String _timezone;

    public Location(String name, double latitude, double longitude, String timezone)
    {
        _name = name;
        _latitude = latitude;
        _longitude = longitude;
        _timezone = timezone;
    }

    public String getName(){return _name;}
    public double getLatitude(){return _latitude;}
    public double getLongitude(){return _longitude;}
    public String getTimezone(){return _timezone;}

    public int describeContents()
    {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags)
    {
        out.writeString(_name);
        out.writeDouble(_latitude);
        out.writeDouble(_longitude);
        out.writeString(_timezone);
    }

    public static final Parcelable.Creator<Location> CREATOR =
            new Parcelable.Creator<Location>()
            {
                public Location createFromParcel(Parcel in)
                {
                    return new Location(in);
                }

                public Location[] newArray(int size)
                {
                    return new Location[size];
                }
            };

    private Location(Parcel in)
    {
        _name = in.readString();
        _latitude = in.readDouble();
        _longitude = in.readDouble();
        _timezone = in.readString();
    }
}
