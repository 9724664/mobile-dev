package swindroid.suntime.calc;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import swindroid.suntime.R;

/**
 * Created by Eden on 11/10/2015.
 */
public class LocationArrayAdapter extends ArrayAdapter<Location> {

    private List<Location> locationsList = new ArrayList<Location>();

    static class ItemViewHolder{
        TextView name;
    }

    public LocationArrayAdapter (Context context, int resource)
    {
        super(context, resource);
    }

    public void add(Location object)
    {
        locationsList.add((Location)object);
        super.add((Location)object);
    }

    public int getCount(){
        return super.getCount();
    }

    public Location getItem(int position)
    {
        return this.locationsList.get(position);
    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        View row = convertView;
        ItemViewHolder viewHolder;

        if(row == null)
        {
            LayoutInflater inflater = (LayoutInflater)this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.single_list_item, parent, false);
            viewHolder = new ItemViewHolder();
            viewHolder.name = (TextView)row.findViewById(R.id.name);
            row.setTag(viewHolder);
        }
        else
        {
            viewHolder = (ItemViewHolder)row.getTag();
        }

        Location loc = getItem(position);
        viewHolder.name.setText(loc.getName() + " - " + loc.getTimezone());
        //viewHolder.name.setTag(position);
        return row;
    }
}
