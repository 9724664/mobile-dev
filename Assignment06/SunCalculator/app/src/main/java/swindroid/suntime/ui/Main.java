package swindroid.suntime.ui;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import swindroid.suntime.AddLocation;
import swindroid.suntime.R;
import swindroid.suntime.calc.AstronomicalCalendar;
import swindroid.suntime.calc.GeoLocation;
import swindroid.suntime.calc.Location;
import swindroid.suntime.calc.LocationArrayAdapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;
import android.widget.ListView;
import android.widget.TextView;

public class Main extends Activity 
{
	GeoLocation currentLocation;
	boolean fileSet;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

		fileSet = fileExists(this.getBaseContext(), "au_locations");

		if (fileSet == false)
		{
			setupFile();
			fileSet = true;
		}
        initializeUI();
    }

	private void initializeUI()
	{
		DatePicker dp = (DatePicker) findViewById(R.id.datePicker);
		Calendar cal = Calendar.getInstance();
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH);
		int day = cal.get(Calendar.DAY_OF_MONTH);
		dp.init(year, month, day, dateChangeHandler); // setup initial values and reg. handler

		if (currentLocation == null)
		{
			TimeZone tz = TimeZone.getDefault();
			currentLocation = new GeoLocation("Melbourne", -37.50, 145.01, tz);
		}

		updateTime(year, month, day);
	}

	public void ButtonAddLocation(View v)
	{
		Intent newAct = new Intent(this, AddLocation.class);
		startActivity(newAct);
	}

	public void ButtonPushed(View v)
	{
		Intent newAct = new Intent(this, LocationSelect.class);
		startActivityForResult(newAct, 0);
	}

	private void updateTime(int year, int monthOfYear, int dayOfMonth)
	{
		//TimeZone tz = TimeZone.getDefault();
		//eoLocation geolocation = new GeoLocation("Melbourne", -37.50, 145.01, tz);
		AstronomicalCalendar ac = new AstronomicalCalendar(currentLocation);
		ac.getCalendar().set(year, monthOfYear, dayOfMonth);
		Date srise = ac.getSunrise();
		Date sset = ac.getSunset();
		
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		
		TextView sunriseTV = (TextView) findViewById(R.id.sunriseTimeTV);
		TextView sunsetTV = (TextView) findViewById(R.id.sunsetTimeTV);
		Log.d("SUNRISE Unformatted", srise+"");
		
		sunriseTV.setText(sdf.format(srise));
		sunsetTV.setText(sdf.format(sset));		
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent intent)
	{
		if(intent == null)
		{

		}
		else {
			Location temp = intent.getExtras().getParcelable("value");

			TimeZone tz = TimeZone.getTimeZone(temp.getTimezone());
			currentLocation = new GeoLocation(temp.getName(),temp.getLatitude(),temp.getLongitude(),tz);

			TextView tv = (TextView)findViewById(R.id.locationTV);
			tv.setText(temp.getName());

			initializeUI();
		}
	}

	OnDateChangedListener dateChangeHandler = new OnDateChangedListener()
	{
		public void onDateChanged(DatePicker dp, int year, int monthOfYear, int dayOfMonth)
		{
			updateTime(year, monthOfYear, dayOfMonth);
		}	
	};

	private boolean fileExists(Context c, String filename)
	{
		File file = c.getFileStreamPath(filename);
		if(file == null || !file.exists())
		{
			return false;
		}
		return true;
	}

	private void setupFile()
	{
		ArrayList<String> readList = new ArrayList<String>();
		InputStream inputStream = getResources().openRawResource(R.raw.au_locations);

		BufferedReader reader = null;
		try
		{
			//FileInputStream inputStream = openFileInput("au_locations");
			reader = new BufferedReader(new InputStreamReader(inputStream));

			String csvLine;

			while((csvLine = reader.readLine()) != null)
			{
				readList.add(csvLine);
			}
		}
		catch ( FileNotFoundException ex)
		{
			throw new RuntimeException("Error reading file:" + ex);
		}
		catch ( IOException ex)
		{
			throw new RuntimeException("Error reading file:" + ex);
		}
		finally
		{
			try
			{
				reader.close();
			}
			catch (IOException e)
			{
				throw new RuntimeException("Error closing input stream: " + e);
			}
		}

		BufferedWriter bufferWriter = null;

		try
		{
			FileOutputStream fileOutputStream = openFileOutput("au_locations", Context.MODE_PRIVATE);
			bufferWriter = new BufferedWriter(new OutputStreamWriter(fileOutputStream));

			for (String line: readList)
			{
				bufferWriter.write(line + "\n");
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				bufferWriter.close();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}

	}
	
}