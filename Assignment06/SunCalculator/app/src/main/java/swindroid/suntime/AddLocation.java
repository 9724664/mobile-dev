package swindroid.suntime;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.Buffer;

public class AddLocation extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_location);

        initUI(savedInstanceState);
    }

    public void initUI(Bundle state)
    {
        if (state == null) {
            return;
        }

        EditText  name = ((EditText)findViewById(R.id.addName));
        EditText  lat = ((EditText)findViewById(R.id.addLat));
        EditText  lon = ((EditText)findViewById(R.id.addLong));
        EditText  tz = ((EditText)findViewById(R.id.addTime));

        name.setText(state.getString("addName"));
        lat.setText(state.getString("addLat"));
        lon.setText(state.getString("addLong"));
        tz.setText(state.getString("addTime"));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_location, menu);
        return true;
    }

    public void ButtonSubmit(View view)
    {

        String name = ((EditText)findViewById(R.id.addName)).getText().toString();
        String lat = ((EditText)findViewById(R.id.addLat)).getText().toString();
        String lon = ((EditText)findViewById(R.id.addLong)).getText().toString();
        String tz = ((EditText)findViewById(R.id.addTime)).getText().toString();

        if(name.trim().length() == 0 || lat.trim().length() == 0 || lon.trim().length() == 0 || tz.trim().length() == 0)
        {
            Toast.makeText(this, "Make sure all fields have data", Toast.LENGTH_LONG).show();
        }
        else
        {
            BufferedWriter bufferWriter = null;

            try
            {
                FileOutputStream fileOutputStream = openFileOutput("au_locations", Context.MODE_APPEND);
                bufferWriter = new BufferedWriter(new OutputStreamWriter(fileOutputStream));
                bufferWriter.write(name + "," + lat + "," + lon + "," + tz + "\n");
                Toast.makeText(this, "Your location was added", Toast.LENGTH_LONG).show();
            }
            catch (FileNotFoundException e)
            {
                e.printStackTrace();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            finally
            {
                try
                {
                    bufferWriter.close();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void onSaveInstanceState(Bundle state)
    {
        String  name = ((EditText)findViewById(R.id.addName)).getText().toString();
        String lat = ((EditText)findViewById(R.id.addLat)).getText().toString();
        String lon = ((EditText)findViewById(R.id.addLong)).getText().toString();
        String tz = ((EditText)findViewById(R.id.addTime)).getText().toString();

        state.putString("addName", name);
        state.putString("addLat", lat);
        state.putString("addLong", lon);
        state.putString("addTime", tz);


        super.onSaveInstanceState(state);
    }
}
