package com.example.eden.imagebrowser;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Eden on 20/09/2015.
 */
public class ImageParce implements Parcelable {

    private String _name;
    private String _url;
    private String _keywords;
    private String _email;
    private String _date;
    private String _rating;
    private int _code;

    public ImageParce (String name, String date, int code)
    {
        _name = name;
        _date = date;
        _code = code;
    }



    public void setName(String name){_name = name;}

    public String getName() {return _name;}
    public String getURL(){ return _url; }
    public String getKeywords() {return _keywords;}
    public String getEmail(){return _email;}
    public String getDate (){return _date;}
    public String getRating(){return _rating;}
    public int getCode(){return _code;}

    public void update (String name, String url, String keywords, String email, String date, String rating)
    {
        _name = name;
        _url = url;
        _keywords = keywords;
        _email = email;
        _date = date;
        _rating = rating;
    }

    public int describeContents()
    {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags)
    {
        out.writeString(_name);
        out.writeString(_url);
        out.writeString(_keywords);
        out.writeString(_email);
        out.writeString(_date);
        out.writeString(_rating);
        out.writeInt(_code);
    }

    public static final Parcelable.Creator<ImageParce> CREATOR =
            new Parcelable.Creator<ImageParce>()
            {
                public ImageParce createFromParcel(Parcel in)
                {
                    return new ImageParce(in);
                }

                public ImageParce[] newArray(int size)
                {
                    return new ImageParce[size];
                }
            };

    /** Private constructor called internally only */
    private ImageParce(Parcel in)
    {
        _name = in.readString();
        _url = in.readString();
        _keywords = in.readString();
        _email = in.readString();
        _date = in.readString();
        _rating = in.readString();
        _code = in.readInt();
    }
}
