package com.example.eden.imagebrowser;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;


public class ImageDisplayActivity extends ActionBarActivity {



    private ImageParce imageParce;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_display);

        if (imageParce == null)
        {
            imageParce = getIntent().getExtras().getParcelable("Value");
        }

        InitUI();
    }

    private void InitUI() {


        EditText name = (EditText) findViewById(R.id.name);
        EditText url = (EditText) findViewById(R.id.url);
        EditText keywords = (EditText) findViewById(R.id.keywords);
        EditText email = (EditText) findViewById(R.id.email);
        EditText date = (EditText) findViewById(R.id.date);
        EditText rating = (EditText) findViewById(R.id.rating);

        name.setText(imageParce.getName());
        url.setText(imageParce.getURL());
        keywords.setText(imageParce.getKeywords());
        email.setText(imageParce.getEmail());
        date.setText(imageParce.getDate());
        rating.setText(imageParce.getRating());
    }


    private void SaveDetails()
    {
        EditText nameText = (EditText) findViewById(R.id.name);
        EditText urlText = (EditText) findViewById(R.id.url);
        EditText keywordsText = (EditText) findViewById(R.id.keywords);
        EditText emailText = (EditText) findViewById(R.id.email);
        EditText dateText = (EditText) findViewById(R.id.date);
        EditText ratingText = (EditText) findViewById(R.id.rating);

        String name = nameText.getText().toString();
        String url = urlText.getText().toString();
        String keywords = keywordsText.getText().toString();
        String email = emailText.getText().toString();
        String date = dateText.getText().toString();
        String rating = ratingText.getText().toString();

        imageParce.update(name, url, keywords, email, date, rating);
    }

    public void onBackPressed()
    {
        SaveDetails();

        Intent result = new Intent();
        result.putExtra("Data", imageParce);
        setResult(RESULT_OK, result);

        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_image_display, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void onSaveInstanceState(Bundle state)
    {
        SaveDetails();

        super.onSaveInstanceState(state);
    }
}
