package com.example.eden.imagebrowser;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity {

    ImageParce image1;
    ImageParce image2;
    ImageParce image3;
    ImageParce image4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (image1 == null)
        {
            image1 = new ImageParce("pizza", "13/01/15", 1);
            image2 = new ImageParce("curry", "14/01/15", 2);
            image3 = new ImageParce("steak", "15/01/15", 3);
            image4 = new ImageParce("rolls", "16/01/15", 4);
        }

        InitUI();

    }

    private void InitUI ()
    {
        TextView image1Name = (TextView) findViewById(R.id.image1Name);
        TextView image2Name = (TextView) findViewById(R.id.image2Name);
        TextView image3Name = (TextView) findViewById(R.id.image3Name);
        TextView image4Name = (TextView) findViewById(R.id.image4Name);
        TextView image1Date = (TextView) findViewById(R.id.image1Date);
        TextView image2Date = (TextView) findViewById(R.id.image2Date);
        TextView image3Date = (TextView) findViewById(R.id.image3Date);
        TextView image4Date = (TextView) findViewById(R.id.image4Date);

        image1Name.setText("Name: " + image1.getName().toString());
        image2Name.setText("Name: " + image2.getName().toString());
        image3Name.setText("Name: " + image3.getName().toString());
        image4Name.setText("Name: " + image4.getName().toString());
        image1Date.setText("Date: " + image1.getDate().toString());
        image2Date.setText("Date: " + image2.getDate().toString());
        image3Date.setText("Date: " + image3.getDate().toString());
        image4Date.setText("Date: " + image4.getDate().toString());
    }


    public void ImageClicked(View v)
    {
        String tag = v.getTag().toString();

        if(tag.equals("image1"))
        {
            Intent newAct = new Intent(this, ImageDisplayActivity.class);
            newAct.putExtra("Value", image1);
            startActivityForResult(newAct, 0);
        }
        else if(tag.equals("image2"))
        {
            Intent newAct = new Intent(this, ImageDisplayActivity.class);
            newAct.putExtra("Value", image2);
            startActivityForResult(newAct, 0);
        }
        else if(tag.equals("image3"))
        {
            Intent newAct = new Intent(this, ImageDisplayActivity.class);
            newAct.putExtra("Value", image3);
            startActivityForResult(newAct, 0);
        }
        else if(tag.equals("image4"))
        {
            Intent newAct = new Intent(this, ImageDisplayActivity.class);
            newAct.putExtra("Value", image4);
            startActivityForResult(newAct, 0);
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent)
    {
        if(intent == null)
            Log.i("ON-ACTIVITY-Intent", "IS NULL");
        else
        {
            ImageParce temp = intent.getExtras().getParcelable("Data");

            switch (temp.getCode())
            {
                case 1: image1 = temp;
                    break;
                case 2: image2 = temp;
                    break;
                case 3: image3 = temp;
                    break;
                case 4: image4 = temp;
                    break;
            }

            InitUI();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
