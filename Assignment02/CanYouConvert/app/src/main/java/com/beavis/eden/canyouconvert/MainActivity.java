package com.beavis.eden.canyouconvert;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.util.Log;


public class MainActivity extends ActionBarActivity {

    private boolean litres = false;
    private TextView textOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    public void ButtonClicked(View view)
    {
        //Button button = (Button) view;

        textOut = (TextView) findViewById(R.id.txtOutput);

        String sCups = ((EditText)findViewById(R.id.cups)).getText().toString();
        String sTea = ((EditText)findViewById(R.id.teaspoons)).getText().toString();
        String sTable = ((EditText)findViewById(R.id.tablespoons)).getText().toString();

        Double cups = Double.parseDouble(sCups);
        Double tea = Double.parseDouble(sTea);
        Double table = Double.parseDouble(sTable);

        cups = cups * 236.58;
        tea = tea * 4.92;
        table = table * 14.78;

        double result = cups + tea + table;

        if (litres == true)
        {
            result = result/1000;
            String fResult = Double.toString(result);
            textOut.setText(fResult + " L");
        }
        else
        {
            String fResult = Double.toString(result);
            textOut.setText(fResult + " ml");
        }
    }

    public void CheckboxClicked(View view)
    {
        if(litres == false)
        {
            litres = true;
        }
        else
        {
            litres = false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
